
@import Foundation;
@import CoreLocation;

#ifndef CONTACTS_FRAMEWORK_AVAILABLE
    #define CONTACTS_FRAMEWORK_AVAILABLE !TARGET_OS_TV
#endif

#if CONTACTS_FRAMEWORK_AVAILABLE
@import Contacts;
#endif


NS_ASSUME_NONNULL_BEGIN

/**
 *  Model object holding a postal address.
 *  It can be initialized using a CNPostalAddress, CLPlacemark, or
 *  by setting either fullAddress or each address component.
 */
@interface AAFAddress : NSObject

/**
 String containing a full formatted address. Use this only if you don't have the different address components.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *fullAddress;

/**
 Street address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *street;

/**
 House number address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *houseNumber;

/**
 Combined street and house number based on the locale of the countryCode property.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *streetWithHouseNumber;

/**
 postal code address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *postalCode;

/**
 City address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *city;

/**
 State address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *state;

/**
 subAdministrativeArea address also known as county. E.g. "Santa Clara", "München Landkreis"
 */
@property (nonatomic, copy, readwrite, nullable) NSString *subAdministrativeArea;

/**
 subAdministrativeArea address component. e.g. "Parkstadt Schwabing"
 */
@property (nonatomic, copy, readwrite, nullable) NSString *subLocality;

/**
 Country address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *country;

/**
 Country code ISO 3166-1 alpha-2 address component.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *countryCode;


#if CONTACTS_FRAMEWORK_AVAILABLE
@property (nonatomic, strong, readonly, nullable) CNPostalAddress *postalAddress;
#endif


/**
 The string used for forward geocoding using the AAFGeocoder.
 fullAddress is used if it is set, else street, zip, city, state and country are concatenated.
 */
@property (nonatomic, copy, readonly) NSString *geolocatorString;

/**
 String containing a formatted address.
 fullAddress is used if it is set, else the result of
 formatting the address using a CNPostalAddressFormatter.
 */
@property (nonatomic, copy, readonly, nullable) NSString *formattedAddress;

/**
 String containing a formatted address including the country.
 fullAddress is used if it is set, else the result of
 formatting the address using a CNPostalAddressFormatter.
 */
@property (nonatomic, copy, readonly, nullable) NSString *formattedAddressWithCountry;

/**
 String containing a formatted street with house number.
 streetWithHouseNumber is used if it is set, else the result of
 concatenating street + house number joined by a separator.
 */
@property (nonatomic, copy, readonly, nullable) NSString *formattedStreetWithHouseNumber;

/**
 String containing a user entered label for the address.
 */
@property (nonatomic, copy, readwrite, nullable) NSString *label;

/**
 Initialize an AAFAddress object using the address components provided by an CLPlacemark
 @param placemark to use for the initialization
 @return AAFAddress initialized with each available address component
 */
- (instancetype)initWithCLPlacemark:(CLPlacemark *)placemark;

#if CONTACTS_FRAMEWORK_AVAILABLE
- (instancetype)initWithPostalAddress:(CNPostalAddress *)address;

- (instancetype)initWithPostalAddress:(CNPostalAddress *)address label:(nullable NSString *)label;
#endif

@end

NS_ASSUME_NONNULL_END
