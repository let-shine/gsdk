
/**
 *  Forward and reverse Geocoder with persistent internal cache and optional manual geolocations.
 *  The Audi Werksverortung is shipped with the Audi MMI Connect app and can be included in your app too.
 *  AAFGeocoder implements the AAFStrategyAdapterRegistry-Pattern and allows the geocoding to be extended with different strategies.
 *  This way you can use a different provider for your geolocation data. To do this you need to implement a custom AAFGeocoderStrategy,
 *  register it on the AAFGeocoder and activate it. Your strategy will leverage all the caching implemented inside AAFGeocoder.
 *
 *  @bug Cache is not cleared if the device language changes.
 */
@import CoreLocation;

#ifndef CONTACTS_FRAMEWORK_AVAILABLE
    #define CONTACTS_FRAMEWORK_AVAILABLE !TARGET_OS_TV
#endif

#if CONTACTS_FRAMEWORK_AVAILABLE
@import Contacts;
#endif
#import <GeoKit/AAFGeocoderStrategy.h>
#import <GeoKit/AAFAddress.h>

@class AAFPlacemark;
#import <GeoKit/AAFGeocoderRegion.h>

NS_ASSUME_NONNULL_BEGIN

extern NSErrorDomain const AAFGeoKitErrorDomain;

typedef NS_ERROR_ENUM(AAFGeoKitErrorDomain, AAFGeoKitErrorCode) {
    AAFGeoKitErrorCodeUnknownStrategy = 1,
    AAFGeoKitErrorCodeStrategyAlreadyRegistered = 2,
    AAFGeoKitErrorCodeStrategyInvalidName = 3,
};

/*
 Completion handler type for all geocoding operations.
 @param results NSArray containing all found AAFPlacemark objects.
 @param error Optional NSError if the geocoding failed.
 */
typedef void (^AAFGeocoderCompletionHandler)(NSArray<AAFPlacemark *> *__nullable results, NSError *__nullable error);

@interface AAFGeocoder : NSObject

/**
 NSArray holding all the registered strategies.
 */
@property (nonatomic, copy, readonly) NSArray<NSString *> *registeredStrategies;

/**
 Current active strategy object.
 */
@property (nonatomic, strong, readonly) id<AAFGeocoderStrategy> activeStrategy;

/**
 Registers an additional strategy.
 @param strategy Strategy to use.
 */
- (BOOL)registerStrategy:(id<AAFGeocoderStrategy>)strategy error:(NSError *__nullable *__nullable)error;

/**
 Deregisters a strategy.
 @param strategy Strategy to use.
 */
- (void)deregisterStrategy:(id<AAFGeocoderStrategy>)strategy;

/**
 @param strategyName Name of the strategy to activate.
 @param error If this method returns NO, an NSError will be returned by reference in the 'error' parameter.
 @return BOOL Returns NO if the passed strategyName could not be resolved to a registered strategy, otherwise YES.
 */

- (BOOL)activateStrategyWithName:(NSString *)strategyName
                           error:(NSError *__nullable *__nullable)error;

/**
 Returns the shared instance.
 */
+ (AAFGeocoder *)sharedGeocoder NS_SWIFT_NAME(sharedGeocoder());

/**
 Forward geocoding converting an address to a location.
 @param addressString Address to be used for geocoding. If you are using an AAFAddress it is recommended to use the geolocatorString.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForAddressString:(NSString *)addressString
               completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:completion:));

/**
 Forward geocoding converting an address to a location.
 @param addressString Address to be used for geocoding. If you are using an AAFAddress it is recommended to use the geolocatorString.
 @param region The region to use in the conversion to prioritize search requests. Search results are not restricted to the region, but search results in or near the region will have higher priority in the list of results.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForAddressString:(NSString *)addressString
                          region:(nullable AAFGeocoderRegion *)region
               completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:region:completion:));

/**
 Forward geocoding converting an address to a location. (Convenience method)
 @param address Address to be used for geocoding.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForAddress:(AAFAddress *)address
         completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:completion:));

/**
 Forward geocoding converting an address to a location. (Convenience method)
 @param address Address to be used for geocoding.
 @param region The region to use in the conversion to prioritize search requests. Search results are not restricted to the region, but search results in or near the region will have higher priority in the list of results.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForAddress:(AAFAddress *)address
                    region:(nullable AAFGeocoderRegion *)region
         completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:region:completion:));


#if CONTACTS_FRAMEWORK_AVAILABLE

/**
 Forward geocoding converting an address to a location. (Convenience method)
 @param postalAddress Address to be used for geocoding.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForPostalAddress:(CNPostalAddress *)postalAddress
               completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:completion:));


/**
 Forward geocoding converting an address to a location. (Convenience method)
 @param postalAddress Address to be used for geocoding.
 @param region The region to use in the conversion to prioritize search requests. Search results are not restricted to the region, but search results in or near the region will have higher priority in the list of results.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)locationForPostalAddress:(CNPostalAddress *)postalAddress
                          region:(nullable AAFGeocoderRegion *)region
               completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(location(for:region:completion:));
#endif

/**
 Reverse geocoding converting a location to an address.
 @param location CLLocation to be used for geocoding.
 @param completionHandler Completion handler for returning the geocoder results. @see AAFGeocoderCompletionHandler.
 */
- (void)addressForLocation:(CLLocation *)location
         completionHandler:(AAFGeocoderCompletionHandler)completionHandler NS_SWIFT_NAME(address(for:completion:));

/**
 Add a given AAFPlacemark object to the internal cache so that it is used if a geocoding operation is requested for the given address or location inside the AAFPlacemark.
 @param placemark AAFPlacemark to be added to the cache.
 */
- (void)addPlacemark:(AAFPlacemark *)placemark;


/**
 Clears the local cache.
 */
- (void)clearCache;

@end

NS_ASSUME_NONNULL_END
