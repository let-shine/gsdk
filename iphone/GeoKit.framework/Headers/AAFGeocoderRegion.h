
@import CoreLocation;

@interface AAFGeocoderRegion : NSObject

@property (nonatomic, readonly, assign) CLLocationDegrees latitude;
@property (nonatomic, readonly, assign) CLLocationDegrees longitude;
@property (nonatomic, readonly, assign) CLLocationDistance radius;

+ (instancetype) regionWithLatitude:(CLLocationDegrees) latitude longitude:(CLLocationDegrees) longitude radius:(CLLocationDistance) radius;

@end
