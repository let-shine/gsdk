
@import CoreLocation;

@class AAFPlacemark;
#import <GeoKit/AAFGeocoderRegion.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const AAFStrategyAdapterDefaultStrategyName;

/**
 This class represents the basic strategy protocol utilized by the AAFGeocoder.
 */

@protocol AAFGeocoderStrategy <NSObject>


/**
 Name of the strategy.
 */
@property (nonatomic, copy, readonly) NSString *strategyName;

/** 
 The number of concurrently running geocoding requests.
 */
@property (nonatomic, assign, readonly) NSInteger maxConcurrentOperationCount;

/**
 A string identifying the localization language. Used to distinguish results languages for caching.
 */
@property (nonatomic, strong, readonly) NSString *localizationLanguage;

/**
 Forward geocoding converting a given address into a location
 @param addressString @c NSString representing the address to geocode. Components are normally separated by commas.
 @param region Optional. The @c CLRegion to use in the conversion to prioritize search requests. Search results are not restricted to the region, but search results in or near the region will have higher priority in the list of results.
 @param error @c NSError pointer to fill if an error occurs
 */
- (nullable NSArray<AAFPlacemark *> *)locationForAddressWithString:(NSString *)addressString
                                                            region:(nullable AAFGeocoderRegion *)region
                                                             error:(NSError **)error;

/**
 Reverse geocoding converting a given location into an address.
 @param location @c CLLocation representing the location to geocode.
 @param error @c NSError pointer to fill if an error occurs
 */
- (nullable NSArray<AAFPlacemark *> *)addressForLocation:(CLLocation *)location
                                                   error:(NSError **)error;

@end

NS_ASSUME_NONNULL_END
