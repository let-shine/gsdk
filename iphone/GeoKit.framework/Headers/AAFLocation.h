
@import CoreLocation;

typedef NS_ENUM(NSUInteger, AAFLocationGeodeticDatum) {
    AAFLocationGeodeticDatumWGS84 = 0,
    AAFLocationGeodeticDatumGCJ02,
    AAFLocationGeodeticDatumAutoCorrected
};

typedef struct
{
    CLLocationDegrees latitude;
    CLLocationDegrees longitude;
    AAFLocationGeodeticDatum geodeticDatum;

} AAFLocationCoordinate;

static AAFLocationCoordinate AAFLocationCoordinateMake(CLLocationDegrees latitude,
                                                       CLLocationDegrees longitude,
                                                       AAFLocationGeodeticDatum geodeticDatum)
{
    AAFLocationCoordinate coordinate = {};
    coordinate.latitude = latitude;
    coordinate.longitude = longitude;
    coordinate.geodeticDatum = geodeticDatum;
    return coordinate;
}

@interface AAFLocation : CLLocation

+ (instancetype)locationWithCoordinate:(AAFLocationCoordinate)coordinate;

+ (instancetype)locationWithCoordinate:(AAFLocationCoordinate)coordinate
                    horizontalAccuracy:(CLLocationAccuracy)horizontalAccuracy;

@end

@interface CLLocation (AAFLocation)

@property (nonatomic, assign, readonly) AAFLocationGeodeticDatum geodeticDatum;
@property (nonatomic, strong, readonly) CLLocation *wgs84Location;
@property (nonatomic, strong, readonly) CLLocation *gcj02Location;
@property (nonatomic, strong, readonly) CLLocation *mapDisplayLocation;

@end
