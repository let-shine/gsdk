
@import Foundation;
@import MapKit;

typedef struct
{
    CLLocationCoordinate2D northEast;

    CLLocationCoordinate2D southWest;
} AAFRectangularRegion;

@interface AAFMapUtility : NSObject

+ (MKCoordinateRegion)convertAAFRectangularRegionToMKCoordinateRegion:(AAFRectangularRegion)region;

+ (AAFRectangularRegion)convertMKCoordinateRegionToAAFRectangularRegion:(MKCoordinateRegion)region;

+ (MKMapRect)convertMKCoordinateRegionToMKMapRect:(MKCoordinateRegion)region;

/// Calculates a target coordinate when moving from an origin location in a specified heading for a specified distance.
/// @param bearingInDegrees The bearing in degrees.
/// @param distanceMeters The distance in meters.
/// @param origin The origin.
+ (CLLocationCoordinate2D)locationWithBearingDegrees:(CLLocationDegrees)bearingInDegrees
                                            distance:(CLLocationDistance)distanceMeters
                                        fromLocation:(CLLocationCoordinate2D)origin;

/// Calculates a target coordinate when moving from an origin location in a specified heading for a specified distance.
/// @param bearingInRadians The bearing in radians.
/// @param distanceMeters The distance in meters.
/// @param origin The origin.
+ (CLLocationCoordinate2D)locationWithBearing:(double)bearingInRadians
                                     distance:(CLLocationDistance)distanceMeters
                                 fromLocation:(CLLocationCoordinate2D)origin;

+ (CLLocationCoordinate2D)northCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                      radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)northEastCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                          radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)eastCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                     radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)southEastCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                          radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)southCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                      radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)southWestCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                          radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)westCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                     radius:(CLLocationDistance)radius;

+ (CLLocationCoordinate2D)northWestCoordinateForCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                                          radius:(CLLocationDistance)radius;
@end
