
@import Foundation;
@import CoreLocation;
#import <GeoKit/AAFAddress.h>
#import <GeoKit/AAFPlacemarkRegion.h>

NS_ASSUME_NONNULL_BEGIN

/**
 *  Model object holding the mapping between an AAFAddress and a CLLocation.
 */
@interface AAFPlacemark : NSObject

/**
 The address object.
 */
@property (nonatomic, strong, readonly, nullable) AAFAddress *address;

/**
 The location object.
 */
@property (nonatomic, strong, readonly, nullable) CLLocation *location;

/**
 The region containing the address and location
 */
@property (nonatomic, strong, readonly, nullable) AAFPlacemarkRegion *region; 

/**
 Designated initializer.
 @param address see address.
 @param location see location.
 */
- (instancetype)initWithAddress:(nullable AAFAddress *)address
                       location:(nullable CLLocation *)location;

/**
 Designated initializer.
 @param address see address.
 @param location see location.
 @param region see region.
 */
- (instancetype)initWithAddress:(nullable AAFAddress *)address
                       location:(nullable CLLocation *)location
                         region:(nullable AAFPlacemarkRegion *)region;

@end

NS_ASSUME_NONNULL_END
