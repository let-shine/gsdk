
@import Foundation;
@import CoreLocation;


@interface AAFPlacemarkRegion : NSObject

- (BOOL)containsCoordinate:(CLLocationCoordinate2D)coordinate;

@end

@interface AAFRectangularPlacemarkRegion : AAFPlacemarkRegion

@property (nonatomic, assign, readonly) CLLocationCoordinate2D northWest;
@property (nonatomic, assign, readonly) CLLocationCoordinate2D southEast;

+ (instancetype)rectangularPlacemarkRegionWithNorthWest:(CLLocationCoordinate2D)northWest
                                              southEast:(CLLocationCoordinate2D)southEast;

+ (instancetype)rectangularPlacemarkRegionWithNorthEast:(CLLocationCoordinate2D)northEast
                                              southWest:(CLLocationCoordinate2D)southWest;

@end

@interface AAFCircularPlacemarkRegion : AAFPlacemarkRegion

@property (nonatomic, assign, readonly) CLLocationCoordinate2D center;
@property (nonatomic, assign, readonly) CLLocationDistance radius;

+ (instancetype)circularPlacemarkRegionWithCenter:(CLLocationCoordinate2D)center
                                           radius:(CLLocationDistance)radius;

@end
