
@import CoreLocation;

NS_ASSUME_NONNULL_BEGIN

/**
 *  Helper to convert a CLLocation to and from an NSDictionary so it can be persisted.
 *  Note: only latitude and longitude are converted.
 */
@interface CLLocation (AAFDictionary)

/**
 Creates a NSDictionary representation of this CLLocation that can be used to persist inside a property list.
 Only latitude and longitude are converted.
 @return NSDictionary that can be put inside a property list.
 */
- (NSDictionary *)dictionaryFromLocation;

/**
 Creates a CLLoation using a NSDictionary that was used to persist this location.
 @param dict NSDictionary that was previously created using dictionaryFromLocation.
 @return CLLocation with the corresponding coordinates.
 */
+ (nullable CLLocation *)locationFromDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
