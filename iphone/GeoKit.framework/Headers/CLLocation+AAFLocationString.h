
@import CoreLocation;

NS_ASSUME_NONNULL_BEGIN

/**
 *  Helper to convert a CLLocation to and from an NSString.
 */
@interface CLLocation (AAFLocationString)

/**
 @return NSString with coordinates in KML format (`latitude,longitude`).
 */
- (NSString *)locationString;

/**
 @return NSString with coordinates in DMS format (`latdegress° latminutes' latseconds", londegrees° longminutes' lonseconds"`).
 */
- (NSString *)locationStringInDMS;

/**
 @param string NSString with KML format (`latitude,longitude`).
 @return CLLocation object representing the passed coordinates.
 */
+ (nullable CLLocation *)locationFromString:(NSString *)string;

@end

NS_ASSUME_NONNULL_END
