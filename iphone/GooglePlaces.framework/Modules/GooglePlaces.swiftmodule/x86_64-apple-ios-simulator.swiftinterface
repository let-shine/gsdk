// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.2 (swiftlang-1103.0.32.6 clang-1103.0.32.51)
// swift-module-flags: -target x86_64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name GooglePlaces
import Foundation
import HTTPClient
import Logger
import Swift
import Utility
public enum Error : Swift.Error, HTTPClient.ConnectorError {
  case unknownError(Swift.String?)
  case zeroResults(Swift.String?)
  case overQueryLimit(Swift.String?)
  case requestDenied(Swift.String?)
  case invalidRequest(Swift.String?)
  case notFound(Swift.String?)
  case persistencyError(Swift.Error)
  case httpClient(HTTPClient.HTTPError)
  public static func error(from httpError: HTTPClient.HTTPError, for requestIdentifier: HTTPClient.UniqueRequestIdentifier) -> GooglePlaces.Error
}
public struct Location {
  public let coordinate: GooglePlaces.Coordinate
  public let radius: Swift.Int
  public init?(latitude: Swift.Double, longitude: Swift.Double, radius: Swift.Int)
}
public enum Price : Swift.Int, Swift.Codable {
  case free
  case inexpensive
  case moderate
  case expensive
  case veryExpensive
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
public struct Photo : Swift.Equatable {
  public let reference: Swift.String
  public let width: Swift.Int
  public let height: Swift.Int
  public let htmlAttributions: [Swift.String]
  public enum MaximumDimension : Swift.Equatable {
    case original
    case width(Swift.Int)
    case height(Swift.Int)
    public static func == (a: GooglePlaces.Photo.MaximumDimension, b: GooglePlaces.Photo.MaximumDimension) -> Swift.Bool
  }
  public static func == (a: GooglePlaces.Photo, b: GooglePlaces.Photo) -> Swift.Bool
}
public struct Address : Swift.Equatable {
  public let street: Swift.String?
  public let houseNumber: Swift.String?
  public let postalCode: Swift.String?
  public let city: Swift.String?
  public let state: Swift.String?
  public let subAdministrativeArea: Swift.String?
  public let subLocality: Swift.String?
  public let country: Swift.String?
  public let countryCode: Swift.String?
  public let formattedAddress: Swift.String?
  public static func == (a: GooglePlaces.Address, b: GooglePlaces.Address) -> Swift.Bool
}
public struct AddressComponent : Swift.Equatable {
  public let types: [Swift.String]
  public let longName: Swift.String
  public let shortName: Swift.String
  public static func == (a: GooglePlaces.AddressComponent, b: GooglePlaces.AddressComponent) -> Swift.Bool
}
public struct Review : Swift.Equatable {
  public let aspects: [GooglePlaces.Review.AspectRating]?
  public let authorName: Swift.String
  public let authorProfilePhotoURL: Swift.String?
  public let authorURLString: Swift.String?
  public let language: GooglePlaces.Language?
  public let rating: Swift.Double
  public let text: Swift.String
  public let time: Foundation.Date
  public struct AspectRating : Swift.Equatable {
    public let aspectType: GooglePlaces.Review.AspectRating.AspectType
    public let rating: Swift.Double
    public enum AspectType : Swift.String, Swift.Codable {
      case appeal
      case atmosphere
      case decor
      case facilities
      case food
      case overall
      case quality
      case service
      public typealias RawValue = Swift.String
      public init?(rawValue: Swift.String)
      public var rawValue: Swift.String {
        get
      }
    }
    public static func == (a: GooglePlaces.Review.AspectRating, b: GooglePlaces.Review.AspectRating) -> Swift.Bool
  }
  public static func == (a: GooglePlaces.Review, b: GooglePlaces.Review) -> Swift.Bool
}
public struct Coordinate : Swift.Equatable {
  public let latitude: Swift.Double
  public let longitude: Swift.Double
  public init(latitude: Swift.Double, longitude: Swift.Double)
  public static func == (a: GooglePlaces.Coordinate, b: GooglePlaces.Coordinate) -> Swift.Bool
}
public struct Geometry : Swift.Equatable {
  public let coordinate: GooglePlaces.Coordinate
  public let viewport: GooglePlaces.Geometry.Viewport?
  public struct Viewport : Swift.Equatable {
    public let northEast: GooglePlaces.Coordinate
    public let southWest: GooglePlaces.Coordinate
    public static func == (a: GooglePlaces.Geometry.Viewport, b: GooglePlaces.Geometry.Viewport) -> Swift.Bool
  }
  public static func == (a: GooglePlaces.Geometry, b: GooglePlaces.Geometry) -> Swift.Bool
}
public struct OpeningHours : Swift.Equatable {
  public let periods: [GooglePlaces.OpeningHours.Period]?
  public let weekdayTexts: [Swift.String]?
  public struct Period : Swift.Equatable {
    public let open: GooglePlaces.OpeningHours.Period.Pair
    public let close: GooglePlaces.OpeningHours.Period.Pair?
    public struct Pair : Swift.Equatable {
      public enum Day : Swift.Int, Swift.Codable {
        case sunday
        case monday
        case tuesday
        case wednesday
        case thursday
        case friday
        case saturday
        public typealias RawValue = Swift.Int
        public init?(rawValue: Swift.Int)
        public var rawValue: Swift.Int {
          get
        }
      }
      public struct Time : Swift.Equatable {
        public let hour: Swift.Int
        public let minute: Swift.Int
      }
      public let day: GooglePlaces.OpeningHours.Period.Pair.Day
      public let time: GooglePlaces.OpeningHours.Period.Pair.Time
      public static func == (a: GooglePlaces.OpeningHours.Period.Pair, b: GooglePlaces.OpeningHours.Period.Pair) -> Swift.Bool
    }
    public static func == (a: GooglePlaces.OpeningHours.Period, b: GooglePlaces.OpeningHours.Period) -> Swift.Bool
  }
  public static func == (a: GooglePlaces.OpeningHours, b: GooglePlaces.OpeningHours) -> Swift.Bool
}
public struct AlternativeId : Swift.Equatable {
  public let placeId: Swift.String
  public let scope: Swift.String
  public static func == (a: GooglePlaces.AlternativeId, b: GooglePlaces.AlternativeId) -> Swift.Bool
}
public struct Place : Swift.Equatable {
  public let placeId: Swift.String
  public let name: Swift.String?
  public let address: GooglePlaces.Address?
  public let addressComponents: [GooglePlaces.AddressComponent]?
  public let geometry: GooglePlaces.Geometry?
  public let vicinity: Swift.String?
  public let permanentlyClosed: Swift.Bool?
  public var placeDetails: GooglePlaces.PlaceDetails? {
    get
  }
  public func addingPlaceDetails(_ details: GooglePlaces.PlaceDetails?) -> GooglePlaces.Place
  public static func == (a: GooglePlaces.Place, b: GooglePlaces.Place) -> Swift.Bool
}
public struct PlaceDetails : Swift.Equatable {
  public let formattedPhoneNumber: Swift.String?
  public let internationalPhoneNumber: Swift.String?
  public let iconURLString: Swift.String?
  public let openingHours: GooglePlaces.OpeningHours?
  public let photos: [GooglePlaces.Photo]?
  public let scope: Swift.String?
  public let alternativeIds: [GooglePlaces.AlternativeId]?
  public let priceLevel: GooglePlaces.Price?
  public let rating: Swift.Double?
  public let numberOfUserRatings: Swift.Int?
  public let reviews: [GooglePlaces.Review]?
  public let types: [GooglePlaces.PlaceType]?
  public let urlString: Swift.String?
  public let utcOffset: Swift.Int?
  public let website: Swift.String?
  public let htmlAttributions: [Swift.String]?
  public static func == (a: GooglePlaces.PlaceDetails, b: GooglePlaces.PlaceDetails) -> Swift.Bool
}
public struct SearchResults : Swift.Equatable {
  public let nextPageToken: Swift.String?
  public let results: [GooglePlaces.SearchResult]
  public let htmlAttributions: [Swift.String]?
  public static func == (a: GooglePlaces.SearchResults, b: GooglePlaces.SearchResults) -> Swift.Bool
}
public struct SearchResult : Swift.Equatable {
  public let place: GooglePlaces.Place
  public let isOpenNow: Swift.Bool?
  public static func == (a: GooglePlaces.SearchResult, b: GooglePlaces.SearchResult) -> Swift.Bool
}
extension Place {
  public enum OpeningState {
    case alwaysOpen
    case open
    case close
    case permanentlyClosed
    case unknown
    public static func == (a: GooglePlaces.Place.OpeningState, b: GooglePlaces.Place.OpeningState) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  public var openingState: GooglePlaces.Place.OpeningState {
    get
  }
  public func openingState(for date: Foundation.Date) -> GooglePlaces.Place.OpeningState
}
extension Address : Utility.Serializable, Utility.Deserializable {
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension AddressComponent : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension AlternativeId : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension OpeningHours.Period.Pair.Day : Utility.JSONDecodable {
}
extension OpeningHours.Period.Pair.Time : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable, Swift.Comparable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
  public static func == (lhs: GooglePlaces.OpeningHours.Period.Pair.Time, rhs: GooglePlaces.OpeningHours.Period.Pair.Time) -> Swift.Bool
  public static func < (lhs: GooglePlaces.OpeningHours.Period.Pair.Time, rhs: GooglePlaces.OpeningHours.Period.Pair.Time) -> Swift.Bool
}
extension OpeningHours.Period.Pair : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension OpeningHours.Period : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension OpeningHours : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Coordinate : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Geometry.Viewport : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Geometry : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Review.AspectRating.AspectType : Utility.JSONDecodable {
}
extension Review.AspectRating : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Review : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Price : Utility.JSONDecodable {
}
extension Photo : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension Place : Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON, placeDetails: GooglePlaces.PlaceDetails?) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
extension PlaceDetails : Utility.JSONDecodable, Utility.Serializable, Utility.Deserializable {
  public init(json: Utility.JSON) throws
  public init(serialized: Utility.Serialized) throws
  public func serialized() -> Utility.Serialized
}
public struct Language : Swift.RawRepresentable, Swift.Codable, Swift.Equatable, Utility.JSONDecodable {
  public typealias RawValue = Swift.String
  public let rawValue: Swift.String
  public init(rawValue: Swift.String)
  public init(_ rawValue: Swift.String)
  public static let arabic: GooglePlaces.Language
  public static let bulgarian: GooglePlaces.Language
  public static let bengali: GooglePlaces.Language
  public static let catalan: GooglePlaces.Language
  public static let czech: GooglePlaces.Language
  public static let danish: GooglePlaces.Language
  public static let german: GooglePlaces.Language
  public static let greek: GooglePlaces.Language
  public static let english: GooglePlaces.Language
  public static let englishAustralian: GooglePlaces.Language
  public static let englishGreatBritain: GooglePlaces.Language
  public static let spanish: GooglePlaces.Language
  public static let basque: GooglePlaces.Language
  public static let farsi: GooglePlaces.Language
  public static let finnish: GooglePlaces.Language
  public static let filipino: GooglePlaces.Language
  public static let french: GooglePlaces.Language
  public static let galician: GooglePlaces.Language
  public static let gujarati: GooglePlaces.Language
  public static let hindi: GooglePlaces.Language
  public static let croatian: GooglePlaces.Language
  public static let hungarian: GooglePlaces.Language
  public static let indonesian: GooglePlaces.Language
  public static let italian: GooglePlaces.Language
  public static let hebrew: GooglePlaces.Language
  public static let japanese: GooglePlaces.Language
  public static let kannada: GooglePlaces.Language
  public static let korean: GooglePlaces.Language
  public static let lithuanian: GooglePlaces.Language
  public static let latvian: GooglePlaces.Language
  public static let malayalam: GooglePlaces.Language
  public static let marathi: GooglePlaces.Language
  public static let dutch: GooglePlaces.Language
  public static let norwegian: GooglePlaces.Language
  public static let polish: GooglePlaces.Language
  public static let portuguese: GooglePlaces.Language
  public static let portugueseBrazil: GooglePlaces.Language
  public static let portuguesePortugal: GooglePlaces.Language
  public static let romanian: GooglePlaces.Language
  public static let russian: GooglePlaces.Language
  public static let slovak: GooglePlaces.Language
  public static let slovenian: GooglePlaces.Language
  public static let serbian: GooglePlaces.Language
  public static let swedish: GooglePlaces.Language
  public static let tamil: GooglePlaces.Language
  public static let telugu: GooglePlaces.Language
  public static let thai: GooglePlaces.Language
  public static let tagalog: GooglePlaces.Language
  public static let turkish: GooglePlaces.Language
  public static let ukrainian: GooglePlaces.Language
  public static let vietnamese: GooglePlaces.Language
  public static let chineseSimplified: GooglePlaces.Language
  public static let chineseTraditional: GooglePlaces.Language
}
public struct PlaceType : Swift.RawRepresentable, Swift.Equatable, Swift.Codable, Utility.JSONDecodable {
  public typealias RawValue = Swift.String
  public let rawValue: Swift.String
  public init(rawValue: Swift.String)
  public init(_ rawValue: Swift.String)
  public static let accounting: GooglePlaces.PlaceType
  public static let airport: GooglePlaces.PlaceType
  public static let amusementPark: GooglePlaces.PlaceType
  public static let aquarium: GooglePlaces.PlaceType
  public static let artGallery: GooglePlaces.PlaceType
  public static let atm: GooglePlaces.PlaceType
  public static let bakery: GooglePlaces.PlaceType
  public static let bank: GooglePlaces.PlaceType
  public static let bar: GooglePlaces.PlaceType
  public static let beautySalon: GooglePlaces.PlaceType
  public static let bicycleStore: GooglePlaces.PlaceType
  public static let bookStore: GooglePlaces.PlaceType
  public static let bowlingAlley: GooglePlaces.PlaceType
  public static let busStation: GooglePlaces.PlaceType
  public static let cafe: GooglePlaces.PlaceType
  public static let campground: GooglePlaces.PlaceType
  public static let carDealer: GooglePlaces.PlaceType
  public static let carRental: GooglePlaces.PlaceType
  public static let carRepair: GooglePlaces.PlaceType
  public static let carWash: GooglePlaces.PlaceType
  public static let casino: GooglePlaces.PlaceType
  public static let cemetery: GooglePlaces.PlaceType
  public static let church: GooglePlaces.PlaceType
  public static let cityHall: GooglePlaces.PlaceType
  public static let clothingStore: GooglePlaces.PlaceType
  public static let convenienceStore: GooglePlaces.PlaceType
  public static let courthouse: GooglePlaces.PlaceType
  public static let dentist: GooglePlaces.PlaceType
  public static let departmentStore: GooglePlaces.PlaceType
  public static let doctor: GooglePlaces.PlaceType
  public static let electrician: GooglePlaces.PlaceType
  public static let electronicsStore: GooglePlaces.PlaceType
  public static let embassy: GooglePlaces.PlaceType
  public static let fireStation: GooglePlaces.PlaceType
  public static let florist: GooglePlaces.PlaceType
  public static let funeralHome: GooglePlaces.PlaceType
  public static let furnitureStore: GooglePlaces.PlaceType
  public static let gasStation: GooglePlaces.PlaceType
  public static let gym: GooglePlaces.PlaceType
  public static let hairCare: GooglePlaces.PlaceType
  public static let hardwareStore: GooglePlaces.PlaceType
  public static let hinduTemple: GooglePlaces.PlaceType
  public static let homeGoodsStore: GooglePlaces.PlaceType
  public static let hospital: GooglePlaces.PlaceType
  public static let insuranceAgency: GooglePlaces.PlaceType
  public static let jewelryStore: GooglePlaces.PlaceType
  public static let laundry: GooglePlaces.PlaceType
  public static let lawyer: GooglePlaces.PlaceType
  public static let library: GooglePlaces.PlaceType
  public static let liquorStore: GooglePlaces.PlaceType
  public static let localGovernmentOffice: GooglePlaces.PlaceType
  public static let locksmith: GooglePlaces.PlaceType
  public static let lodging: GooglePlaces.PlaceType
  public static let mealDelivery: GooglePlaces.PlaceType
  public static let mealTakeaway: GooglePlaces.PlaceType
  public static let mosque: GooglePlaces.PlaceType
  public static let movieRental: GooglePlaces.PlaceType
  public static let movieTheater: GooglePlaces.PlaceType
  public static let movingCompany: GooglePlaces.PlaceType
  public static let museum: GooglePlaces.PlaceType
  public static let nightClub: GooglePlaces.PlaceType
  public static let painter: GooglePlaces.PlaceType
  public static let park: GooglePlaces.PlaceType
  public static let parking: GooglePlaces.PlaceType
  public static let petStore: GooglePlaces.PlaceType
  public static let pharmacy: GooglePlaces.PlaceType
  public static let physiotherapist: GooglePlaces.PlaceType
  public static let plumber: GooglePlaces.PlaceType
  public static let police: GooglePlaces.PlaceType
  public static let postOffice: GooglePlaces.PlaceType
  public static let realEstateAgency: GooglePlaces.PlaceType
  public static let restaurant: GooglePlaces.PlaceType
  public static let roofingContractor: GooglePlaces.PlaceType
  public static let rvPark: GooglePlaces.PlaceType
  public static let school: GooglePlaces.PlaceType
  public static let shoeStore: GooglePlaces.PlaceType
  public static let shoppingMall: GooglePlaces.PlaceType
  public static let spa: GooglePlaces.PlaceType
  public static let stadium: GooglePlaces.PlaceType
  public static let storage: GooglePlaces.PlaceType
  public static let store: GooglePlaces.PlaceType
  public static let subwayStation: GooglePlaces.PlaceType
  public static let supermarket: GooglePlaces.PlaceType
  public static let synagogue: GooglePlaces.PlaceType
  public static let taxiStand: GooglePlaces.PlaceType
  public static let trainStation: GooglePlaces.PlaceType
  public static let transitStation: GooglePlaces.PlaceType
  public static let travelAgency: GooglePlaces.PlaceType
  public static let university: GooglePlaces.PlaceType
  public static let veterinaryCare: GooglePlaces.PlaceType
  public static let zoo: GooglePlaces.PlaceType
  public static let administrativeAreaLevel1: GooglePlaces.PlaceType
  public static let administrativeAreaLevel2: GooglePlaces.PlaceType
  public static let administrativeAreaLevel3: GooglePlaces.PlaceType
  public static let administrativeAreaLevel4: GooglePlaces.PlaceType
  public static let administrativeAreaLevel5: GooglePlaces.PlaceType
  public static let colloquialArea: GooglePlaces.PlaceType
  public static let country: GooglePlaces.PlaceType
  public static let establishment: GooglePlaces.PlaceType
  public static let finance: GooglePlaces.PlaceType
  public static let floor: GooglePlaces.PlaceType
  public static let food: GooglePlaces.PlaceType
  public static let generalContractor: GooglePlaces.PlaceType
  public static let geocode: GooglePlaces.PlaceType
  public static let health: GooglePlaces.PlaceType
  public static let intersection: GooglePlaces.PlaceType
  public static let locality: GooglePlaces.PlaceType
  public static let naturalFeature: GooglePlaces.PlaceType
  public static let neighborhood: GooglePlaces.PlaceType
  public static let placeOfWorship: GooglePlaces.PlaceType
  public static let political: GooglePlaces.PlaceType
  public static let pointOfInterest: GooglePlaces.PlaceType
  public static let postBox: GooglePlaces.PlaceType
  public static let postalCode: GooglePlaces.PlaceType
  public static let postalCodePrefix: GooglePlaces.PlaceType
  public static let postalCodeSuffix: GooglePlaces.PlaceType
  public static let postalTown: GooglePlaces.PlaceType
  public static let premise: GooglePlaces.PlaceType
  public static let room: GooglePlaces.PlaceType
  public static let route: GooglePlaces.PlaceType
  public static let streetAddress: GooglePlaces.PlaceType
  public static let streetNumber: GooglePlaces.PlaceType
  public static let sublocality: GooglePlaces.PlaceType
  public static let sublocalityLevel4: GooglePlaces.PlaceType
  public static let sublocalityLevel5: GooglePlaces.PlaceType
  public static let sublocalityLevel3: GooglePlaces.PlaceType
  public static let sublocalityLevel2: GooglePlaces.PlaceType
  public static let sublocalityLevel1: GooglePlaces.PlaceType
  public static let subpremise: GooglePlaces.PlaceType
  public var requestTypes: [GooglePlaces.PlaceType] {
    get
  }
  public var isRequestType: Swift.Bool {
    get
  }
}
final public class Client {
  public init(key: Swift.String, clientId: Swift.String, client: HTTPClient.Client = HTTPClient.Client(requestManager: URLSessionRequestManager()))
  final public func searchPlaces(for query: Swift.String, language: GooglePlaces.Language, location: GooglePlaces.Location? = nil, min: GooglePlaces.Price? = nil, max: GooglePlaces.Price? = nil, isOpenNow: Swift.Bool? = nil, type: GooglePlaces.PlaceType? = nil, nextPageToken: Swift.String? = nil, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<GooglePlaces.SearchResults, GooglePlaces.Error>) -> Swift.Void)
  final public func place(for placeId: Swift.String, language: GooglePlaces.Language) -> GooglePlaces.Place?
  final public func placeDetails(for placeId: Swift.String, language: GooglePlaces.Language, ignoreCache: Swift.Bool = false, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<GooglePlaces.Place, GooglePlaces.Error>) -> Swift.Void)
  final public func placeDetails(forCId cId: Swift.String, language: GooglePlaces.Language, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<GooglePlaces.Place, GooglePlaces.Error>) -> Swift.Void)
  final public func photoData(for photo: GooglePlaces.Photo, maxDimension: GooglePlaces.Photo.MaximumDimension = .original, completionQueue: Dispatch.DispatchQueue? = nil, completion: @escaping (Swift.Result<Foundation.Data, GooglePlaces.Error>) -> Swift.Void)
  final public func photoDataURL(for photo: GooglePlaces.Photo, maxDimension: GooglePlaces.Photo.MaximumDimension = .original) throws -> Foundation.URL
  @objc deinit
}
extension Client : HTTPClient.AuthorizationProvider {
  final public func authorization(completion: @escaping (HTTPClient.AuthorizationProviderResult) -> Swift.Void)
}
extension GooglePlaces.Price : Swift.Equatable {}
extension GooglePlaces.Price : Swift.Hashable {}
extension GooglePlaces.Price : Swift.RawRepresentable {}
extension GooglePlaces.Review.AspectRating.AspectType : Swift.Equatable {}
extension GooglePlaces.Review.AspectRating.AspectType : Swift.Hashable {}
extension GooglePlaces.Review.AspectRating.AspectType : Swift.RawRepresentable {}
extension GooglePlaces.OpeningHours.Period.Pair.Day : Swift.Equatable {}
extension GooglePlaces.OpeningHours.Period.Pair.Day : Swift.Hashable {}
extension GooglePlaces.OpeningHours.Period.Pair.Day : Swift.RawRepresentable {}
extension GooglePlaces.Place.OpeningState : Swift.Equatable {}
extension GooglePlaces.Place.OpeningState : Swift.Hashable {}
