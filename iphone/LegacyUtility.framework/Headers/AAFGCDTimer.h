
@import Foundation;

@interface AAFGCDTimer : NSObject

NS_ASSUME_NONNULL_BEGIN

@property (nonatomic, assign, readwrite, getter=isSuspended) BOOL suspended;
@property (nonatomic, assign, readonly, getter=isValid) BOOL valid;

/**
 *  BEWARE: If you schedule a repeating timer:
 *          1. You HAVE TO invalidate the timer, otherwise it will go on forever.
 *          2. You HAVE TO keep a strong reference to the timer, otherwise the timer will be dealloced right after creation
 *
 *          If you schedule a non-repeating timer, you do not have to keep a reference.
 *
 */
+ (AAFGCDTimer *__nullable)scheduledTimerWithInterval:(NSTimeInterval)interval
                                              repeats:(BOOL)repeats
                                         firesOnStart:(BOOL)firesOnStart
                                              handler:(dispatch_block_t)handler;

- (void)invalidate;

@end

NS_ASSUME_NONNULL_END
