
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

/**
 AAFObservers is a means to registry a number of observers (instances all implementing the same oberver protocol) and calling methods from the oberver protocol on all of them at once. The registry supports strong or weak references to the added observers and can optionally ensure that observers are always called on the main thread.
 
 To use the observer registry, an instance of AAFObservers is typically used in the class generating observable calls. The using class forwards addObserver and removeObserver calls to the registry ensuring that registered observers implement the expected protocol.
 
 To notify observers, the registry itself acts as a proxy object, allowing all methods from the observer protocol to be called directly on the registry. The proxy then forwards the invocations to the actual observers (in an undefined order). Therefore, the AAFObservers type should be used in conjunction with the observer protocol (e.g. `AAFObserver<MyObserverProtocol>`). You can then simply call the observer method as if `AAFObserver` would actually conform to the protocol.
 */
@interface AAFObservers : NSObject

/**
 A boolean value indicating whether messages forwarded to the registered observers should be dispatched on the main thread.
 If `NO`, observers are notified immediately on the thread the proxy is invoked on.
 */
@property (nonatomic, assign, readwrite, getter=notifiesObserversOnMainThread) BOOL notifyObserversOnMainThread;

@property (nonatomic, assign, readonly) NSUInteger count;

/**
 Allocates and initializes an observer registry holding strong (retaining) references to all observers.
 Keep in mind that observers are only released, after they have been expclicity removed from the registry or the registry itself is deallocated.
 */
+ (instancetype)weakObservers;

/**
 Allocates and initializes an observer registry holding weak (non-retaining) references to all observers.
 Observers might get deallocated during the lifetime of the observer.
 */
+ (instancetype)strongObservers;

/**
 Adds an observing instance to the receiver. The receiver will hold a strong or weak reference, depending on how it was initialized.
 @param observer An object conforming to an agreed upon observer protocol (should be typechecked by the caller)
 */
- (void)addObserver:(id)observer;

/**
 Removes a previously registered observing instance from the receiver. If the instance has not been registered yet, this method does nothing.
 @param observer An object conforming to an agreed upon observer protocol (should be typechecked by the caller)
 */
- (void)removeObserver:(id)observer;

@end

NS_ASSUME_NONNULL_END
