
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

extern NSString *const AAFPersistentArchiverInvalidObjectException;
extern NSString *const AAFPersistentArchiverReadOnlyPropertyException;
extern NSString *const AAFPersistentArchiverInvalidPersistingObjectPropertyException;
extern NSString *const AAFPersistentArchiverInvalidCollectionPropertyException;
extern NSString *const AAFPersistentArchiverInvalidCollectionPropertyClassException;

typedef NSDictionary<NSString *, id> *AAFPersistentDictionaryRepresentation;

@protocol AAFPersisting<NSObject>

@optional

+ (NSArray<NSString *> *)propertiesToPersist;
+ (NSDictionary<NSString *, Class> *)collectionPropertiesToPersist;

- (void)awakeFromArchive;

@end

@interface AAFPersistentArchiver : NSObject

+ (nullable id<AAFPersisting>)objectFromDictionaryRepresentation:(NSDictionary *)dictionary
                                                       withClass:(Class<AAFPersisting>)objectClass;

+ (AAFPersistentDictionaryRepresentation)dictionaryRepresentationOfObject:(id<AAFPersisting>)object;

+ (NSArray<id> *)objectsFromDictionaryRepresentations:(NSArray *)dictionaryRepresentations
                                            withClass:(Class<AAFPersisting>)objectClass;

+ (NSArray<AAFPersistentDictionaryRepresentation> *)dictionaryRepresentationsOfObjects:(NSArray *)objects;

@end

NS_ASSUME_NONNULL_END
