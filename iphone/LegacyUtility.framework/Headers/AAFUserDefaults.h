
@import Foundation;

typedef NS_OPTIONS(NSUInteger, AAFUserDefaultsPersistencyMode)
{
    AAFUserDefaultsPersistencyModeNone = 0,          /** Does not persist user defaults at all */
    AAFUserDefaultsPersistencyModeSystem = (1 << 0), /** Stores all user defaults in [NSUserDefaults standardUserDefaults] */
#if !TARGET_OS_TV
    AAFUserDefaultsPersistencyModeFile = (1 << 1),              /** Stores user defaults on disk in application support directory plist files */
#endif
};

/** Uses file persistency on iOS and watchOS and uses system on tvOS */
extern const AAFUserDefaultsPersistencyMode AAFUserDefaultsDefaultPersistencyMode;

typedef NS_OPTIONS(NSUInteger, AAFUserDefaultsFileOptions) {
    AAFUserDefaultsFileOptionsNone = 0,
    AAFUserDefaultsFileOptionsProtectionCompleteUntilFirstUserAuthentication= 1 << 0, /** Sets NSDataWritingFileProtectionCompleteUntilFirstUserAuthentication for user defaults stored on disk. Only applicable if AAFUserDefaultsPersistencyModeFile is set, too. */
    AAFUserDefaultsFileOptionsExcludeFileFromBackup = 1 << 1, /** Excludes user defaults stored on disk. Only applicable if AAFUserDefaultsPersistencyModeFile is set, too. */
};

@class AAFUserDefaults;

NS_ASSUME_NONNULL_BEGIN

@protocol AAFUserDefaultsObserver<NSObject>

- (void)didReloadUserDefaults:(AAFUserDefaults *)defaults;

@end

@interface AAFUserDefaults : NSObject

/**
 Get the standard user defaults
 @returns AAFUserDefaults
 */
+ (instancetype)standardUserDefaults;

/**
 Defaults that are saved using the ProtectionCompleteUntilFirstUserAuthentication flag.
*/
+ (instancetype)protectedUserDefaults;

+ (instancetype)userDefaultsForAppGroup:(NSString *__nullable)appGroupIdentifier
                           withFileName:(NSString *)fileName
                            fileOptions:(AAFUserDefaultsFileOptions)fileOptions;

+ (instancetype)userDefaultsForAppGroup:(NSString *__nullable)appGroupIdentifier
                           withFileName:(NSString *)fileName
                            fileOptions:(AAFUserDefaultsFileOptions)fileOptions
                        persistencyMode:(AAFUserDefaultsPersistencyMode)persistencyMode;

@property (nonatomic, assign, readwrite) BOOL readOnly;

/**
 Get the object for the pased key
 @param defaultName Key to be used.
 @returns found object or nil if not found
 */
- (id __nullable)objectForKey:(NSString *)defaultName;

/**
 @param value object to be saved. If value is nil removeObjectForKey: is called.
 @param defaultName Key to be used.
 */
- (void)setObject:(id __nullable)value forKey:(NSString *)defaultName;

/**
 Removes the object for the passed key
 @param defaultName Key to be used.
 */
- (void)removeObjectForKey:(NSString *)defaultName;

/**
 Removes all objects for all keys.
 */
- (void)removeAllObjects;

/**
 Helper for objectForKey: that returns the boolValue of the found object
 @param defaultName Key to be used.
 @returns BOOL value of the object or NO if the object found does not respond to @selector(boolValue)
 */
- (BOOL)boolForKey:(NSString *)defaultName;

/**
 Helper for setObject:forKey: that puts the value inside an NSNumber*
 @param value to be saved
 @param defaultName Key to be used.
 @exception AAFUserDefaultsNoKeyException if no key is passed
 */
- (void)setBool:(BOOL)value forKey:(NSString *)defaultName;

/**
 Save all changes to disk
 */
- (void)synchronize;

/**
 Registers defaults the same as @see NSUserDefaults
 @param registrationDictionary to be saved
 */
- (void)registerDefaults:(NSDictionary *)registrationDictionary;

- (void)registerObserver:(id<AAFUserDefaultsObserver>)observer;
- (void)deregisterObserver:(id<AAFUserDefaultsObserver>)observer;

@end

NS_ASSUME_NONNULL_END
