
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

/**
 Helpers to calculate MD5 and SHA1 hashes
 */
@interface NSString (AAFHash)

/**
 Calculates the MD5 hash of the current string
 @returns NSString containing the MD5 hash
 */
@property (nonatomic, readonly) NSString *md5Hash;

/**
 Calculates the SHA1 hash of the current string
 @returns NSString containing the SHA1 hash
 */
@property (nonatomic, readonly) NSString *sha1Hash;

/**
 Calculates the SHA256 hash of the current string
 @returns NSString containing the SHA256 hash
 */
@property (nonatomic, readonly) NSString *sha256Hash;

/**
 Calculates the MD5 hash of the file at the provided path. This does not load the file into memory so it is safe to use on big files.
 @param path NSString containing the path to the file for which you want to generate the MD5 hash
 @returns NSString containing the MD5 hash of the file at the given path
 */
+ (NSString *__nullable)md5HashForFile:(NSString *)path;

/**
 Calculates the SHA1 hash of the file at the provided path. This does not load the file into memory so it is safe to use on big files.
 @param path NSString containing the path to the file for which you want to generate the SHA1 hash
 @returns NSString containing the SHA1 hash of the file at the given path
 */
+ (NSString *__nullable)sha1HashForFile:(NSString *)path;

/**
 Calculates the SHA256 hash of the file at the provided path. This does not load the file into memory so it is safe to use on big files.
 @param path NSString containing the path to the file for which you want to generate the SHA256 hash
 @returns NSString containing the SHA256 hash of the file at the given path
 */
+ (NSString *__nullable)sha256HashForFile:(NSString *)path;

@end

NS_ASSUME_NONNULL_END
