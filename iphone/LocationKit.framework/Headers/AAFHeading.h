
@import CoreLocation;

NS_ASSUME_NONNULL_BEGIN

@interface AAFHeading : NSObject

@property (nonatomic, assign, readonly) CLLocationDirection trueNorth;

/**
 Contains the (optional) underlying CLHeading. If this heading was not provided by CoreLocation this will be nil.
 */
@property (nonatomic, strong, readonly, nullable) CLHeading *heading;

+ (instancetype)headingWithTrueNorth:(CLLocationDirection)trueNorth;
+ (instancetype)headingWithCLHeading:(CLHeading *)heading;

@end

NS_ASSUME_NONNULL_END
