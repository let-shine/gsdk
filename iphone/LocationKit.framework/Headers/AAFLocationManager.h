
@import Foundation;
@import CoreLocation;
#import "AAFLocationProvider.h"
#import "AAFHeading.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, AAFLocationTrackingMode) {
    AAFLocationTrackingModeForeground = 1 << 0,
    AAFLocationTrackingModeBackground = 1 << 1,
};

typedef void (^AAFLocationsUpdateHandler)(NSArray<__kindof CLLocation *> *locations);
typedef void (^AAFHeadingUpdateHandler)(AAFHeading *heading);

/**
 IMPORTANT: Do NOT use the AAFLocationManager without first registering as a user thereof!
 It _might_ work but is _not_ guaranteed to do so, if for instance the last registered user deregisters.
 */
@interface AAFLocationManager : NSObject

#pragma mark - Properties
/**
 Indicates whether (A)GPS is active or not.
 */
@property (nonatomic, assign, readonly, getter=isTrackingLocation) BOOL trackingLocation;

/**
 Indicates whether heading is currently being tracked or not.
 */
@property (nonatomic, assign, readonly, getter=isTrackingHeading) BOOL trackingHeading;

/**
 Reflects the current authorization status. (I.e. whether the permissions are sufficient to use location services). Observable.
 */
@property (nonatomic, assign, readonly) CLAuthorizationStatus locationAuthorizationStatus;

/**
 Idicates whether the device has a built-in GPS module.
 */
@property (nonatomic, assign, readonly) BOOL hasGPSModule;

#pragma mark - Methods
/**
 @return Returns the sharedLocationManager instance.
 */
+ (instancetype)sharedLocationManager;

/**
 This method adds the tracker object to a list of objects interested in location updates, for the supplied `mode`.
 The accuracy defaults to `kCLLocationAccuracyThreeKilometers`.
 
 @param tracker The location tracking object.
 @param mode The location tracking mode (either foreground, background or both).
 */
- (void)registerLocationTracker:(id)tracker
                forTrackingMode:(AAFLocationTrackingMode)mode;

/**
 This method adds the tracker object to a list of objects interested in location updates, for the supplied `mode` along with the desired `accuracy`.
 
 @param tracker The location tracking object.
 @param mode The location tracking mode (either foreground, background or both).
 @param accuracy The desired accuracy for subsequent location updates.
 */
- (void)registerLocationTracker:(id)tracker
                forTrackingMode:(AAFLocationTrackingMode)mode
                desiredAccuracy:(CLLocationAccuracy)accuracy;

/**
 This method adds the tracker object to a list of objects interested in location updates, for the
 supplied `mode` along with the desired `accuracy`.
 
 @param tracker The location tracking object.
 @param accuracy The desired accuracy for subsequent location updates.
 @param mode The location tracking mode (either foreground, background or both).
 @param locationUpdateHandler Executed whenever the location manager receives updates. If the registration occured on the main thread, the locationChangeHandler will be executed on the main thread as well.
 */
- (void)registerLocationTracker:(id)tracker
                forTrackingMode:(AAFLocationTrackingMode)mode
                desiredAccuracy:(CLLocationAccuracy)accuracy
      locationsDidUpdateHandler:(nullable AAFLocationsUpdateHandler)locationUpdateHandler;

/**
 This method removes the tracker object from the list of objects interested in location updates, regardless of the mode it was subscribed for.
 
 @param tracker The location tracking object.
 */
- (void)deregisterLocationTracker:(id)tracker;

/**
 This methods adds the tracker object to a list of objects interested in heading updates.
 
 @warn Heading updates are only available whenever the application is in the foreground.
 
 @param tracker The heading tracking object.
 */
- (void)registerHeadingTracker:(id)tracker;

/**
 This methods adds the tracker object to a list of objects interested in heading updates. 
 
 @warn Heading updates are only available whenever the application is in the foreground.
 
 @param tracker The location tracking object.
 @param headingChangeHandler Executed whenever the location manager receives heading updates. If the registration occured on the main thread, the headingChangeHandler will be executed on the main thread as well.
 */
- (void)registerHeadingTracker:(id)tracker
       headingDidUpdateHandler:(nullable AAFHeadingUpdateHandler)headingChangeHandler;

/**
 This method removes the tracker object from the list of objects interested in heading updates.
 
 @param tracker The heading tracking object.
 */
- (void)deregisterHeadingTracker:(id)tracker;

/**
 @warn Please note that this method is not guaranteed to return a valid location. If `trackingLocation` is
 set to YES but no accurate (A)GPS reading was made, this method will return `nil`. It might also return nil if
 `trackingLocation` is set to NO and no (A)GPS readings were made before.
 
 @return Returns the most recent location that was determined by the location manager.
 */
- (nullable CLLocation *)mostRecentLocation;

/**
 @warn Please note that this method is not guaranteed to return a valid location. It will return `nil` if no
 location matching your requirements could be found. See `mostRecentLocation` for more information on additional
 circumstances under which this method will return `nil`.
 
 @param seconds The maximum age of the location to retrieve.
 
 @param inaccuracy The maximum (horizontal) inaccuracy (in meteres) that is acceptable for the location to retrieve.
 
 @return Returns the most recent location that was determined by the location manager matching the supplied
 constraints.
 */
- (nullable CLLocation *)bestRecentLocationNoOlderThan:(NSTimeInterval)seconds
                                     maximumInaccuracy:(CLLocationAccuracy)inaccuracy;

/**
 This method adds a Location Provider to the Manager
 
 @param provider Provider that will give the manager new locations
 */
- (void)addProvider:(AAFLocationProvider *)provider;

@end

NS_ASSUME_NONNULL_END
