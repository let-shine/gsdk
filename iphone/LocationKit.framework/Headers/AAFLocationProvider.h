
@import CoreLocation;
#import "AAFHeading.h"

NS_ASSUME_NONNULL_BEGIN

@interface AAFLocationProvider: NSObject

@property (nonatomic, assign, readonly, getter=isThrottelled) BOOL throttelled;

/**
 The flag that says wether or not the provider is tracking the devices location
 */
@property (nonatomic, assign, readonly, getter=isTrackingLocation) BOOL trackingLocation;

/**
 The flag that says wether or not the provider is tracking the devices heading
 */
@property (nonatomic, assign, readonly, getter=isTrackingHeading) BOOL trackingHeading;

/**
 Turn on throttle mode
 */
- (void)throttle NS_REQUIRES_SUPER;

/**
 Turn off throttle mode
 */
- (void)unthrottle NS_REQUIRES_SUPER;

/**
 Throttle everything but this Provider
 */
- (void)throttleEverythingElse NS_REQUIRES_SUPER;

/**
 Set the throtteling settings to it's default
 */
- (void)throttleEverythingButDefault NS_REQUIRES_SUPER;

/**
 Sends Locations to every listener

 @param locations Locations that should be sent to every listener
 */
- (void)provideLocations:(NSArray<CLLocation *> *)locations NS_REQUIRES_SUPER;

/**
 Sends an updated heading to every listener

 @param heading Updated heading that should be sent to every listener
 */
- (void)provideHeading:(AAFHeading *)heading NS_REQUIRES_SUPER;

@end

NS_ASSUME_NONNULL_END
