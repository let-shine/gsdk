// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target x86_64-apple-ios12.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Logger
import Foundation
@_exported import Logger
import Swift
@objc public class AAFLog : ObjectiveC.NSObject {
  @objc public static func log(message: Swift.String, module: Swift.String, level: Logger.Level, containsSensitiveInformation: Swift.Bool, timestamp: Foundation.Date, file: Swift.String, function: Swift.String, line: Swift.UInt)
  @objc override dynamic public init()
  @objc deinit
}
final public class DynamicMessage {
  public init(message: @escaping () -> Swift.String, module: Swift.String = defaultModule, level: Logger.Level = .verbose, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
  @objc deinit
}
public typealias Formatter = (Logger.Message) -> Swift.String
@objc(AAFLogLevel) public enum Level : Swift.Int {
  case error
  case warning
  case info
  case debug
  case verbose
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
public let defaultModule: Swift.String
final public class Log {
  public static func add(sink: Logger.LogSink)
  public static func message(_ message: Logger.DynamicMessage)
  public static let main: Logger.ModuleLog
  public static func flush()
  @objc deinit
}
public class FileLogSink : Logger.LogSink {
  public struct LogFile {
    public let url: Foundation.URL
    public let dateCreated: Foundation.Date
    public var size: Swift.Int {
      get
      }
  }
  public static let defaultLogDirectoryURL: Foundation.URL
  final public let logDirectoryURL: Foundation.URL
  final public let filenamePrefix: Swift.String?
  public var logLevel: Logger.Level
  public var shouldLogSensitiveInformation: Swift.Bool
  public var maximumSizeOfLogFilesDirectory: Swift.Int? {
    get
    set
  }
  public var currentLogFile: Logger.FileLogSink.LogFile? {
    get
    }
  public init(logLevel: Logger.Level, formatter: Logger.Formatter? = nil, sensitiveInformationFormatter: Logger.Formatter? = nil, logDirectoryURL: Foundation.URL = defaultLogDirectoryURL, filenamePrefix: Swift.String? = nil, createNewLogFile: Swift.Bool = true, maximumSizeOfLogFilesDirectory: Swift.Int? = nil)
  public func createNewLogFile()
  public func closeLogFile()
  public func log(_ message: Logger.Message)
  public func flush()
  @objc deinit
}
extension FileLogSink {
  public var logFiles: [Logger.FileLogSink.LogFile]? {
    get
  }
  public func deleteAllClosedLogFiles()
  public func pruneLogFiles()
}
extension FileLogSink.LogFile : Swift.Comparable {
  public static func == (lhs: Logger.FileLogSink.LogFile, rhs: Logger.FileLogSink.LogFile) -> Swift.Bool
  public static func < (lhs: Logger.FileLogSink.LogFile, rhs: Logger.FileLogSink.LogFile) -> Swift.Bool
}
final public class Message {
  final public let message: Swift.String
  final public let module: Swift.String
  final public let level: Logger.Level
  final public let containsSensitiveInformation: Swift.Bool
  final public let timestamp: Foundation.Date
  final public let file: Swift.String
  final public let fileName: Swift.String
  final public let function: Swift.String
  final public let line: Swift.UInt
  final public let threadId: Swift.UInt64
  @objc deinit
}
public struct ModuleLog {
  public let name: Swift.String
  public init(name: Swift.String)
  public func error(_ message: @autoclosure @escaping () -> Swift.String, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
  public func info(_ message: @autoclosure @escaping () -> Swift.String, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
  public func warning(_ message: @autoclosure @escaping () -> Swift.String, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
  public func debug(_ message: @autoclosure @escaping () -> Swift.String, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
  public func verbose(_ message: @autoclosure @escaping () -> Swift.String, containsSensitiveInformation: Swift.Bool = false, timestamp: Foundation.Date = Date(), file: Swift.String = #file, function: Swift.String = #function, line: Swift.UInt = #line)
}
public class ConsoleLogSink : Logger.LogSink {
  convenience public init(logLevel: Logger.Level)
  public init(logLevel: Logger.Level, formatter: @escaping Logger.Formatter)
  public var logLevel: Logger.Level
  public func log(_ message: Logger.Message)
  public func flush()
  @objc deinit
}
public protocol LogSink {
  var logLevel: Logger.Level { get set }
  func log(_ message: Logger.Message)
  func flush()
}
extension Logger.Level : Swift.Equatable {}
extension Logger.Level : Swift.Hashable {}
extension Logger.Level : Swift.RawRepresentable {}
