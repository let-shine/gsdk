
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

typedef void (^AAFReachabilityHostBecameReachable)(NSString *host, BOOL WWANOnly);
typedef void (^AAFReachabilityHostBecameUnreachable)(NSString *host);

typedef void (^AAFReachabilityLocalNetworkBecameReachable)(void);
typedef void (^AAFReachabilityLocalNetworkBecameUnreachable)(void);

/**
 This class allows your app to get information about the current route and react on any route changes.
 This can be used to detect if a certain host is reachable or not. It uses KSReachability internally.
 */
@interface AAFReachability : NSObject

/**
 Default reachability instance
 */
+ (AAFReachability *)defaultReachability;

/**
 Add an observer for a certain host and pass which blocks to call on reachability changes.
 Your blocks will be called immediately after the observer is added with the current reachability status.
 @param observer The observer that wants to register for reachability changes.
 @param host NSString with a hostname or IP address
 @param reachableBlock Block to be called whenever the passed host becomes reachable. WWANOnly is YES if the host is only reachable over the 3G connection.
 @param unreachableBlock Block to be called whenever the passed host becomes unreachable.
 */
- (void)addReachabilityObserver:(id)observer
                        forHost:(NSString *)host
                 reachableBlock:(AAFReachabilityHostBecameReachable)reachableBlock
               unreachableBlock:(AAFReachabilityHostBecameUnreachable)unreachableBlock;

/**
 Remove an observer for the certain host.
 @param observer The observer that wants to unregister for reachability changes.
 @param host NSString with a hostname or IP address
 */
- (void)removeReachabilityObserver:(id)observer
                           forHost:(NSString *)host;

/**
 Add an observer for internet reachability and pass which blocks to call on reachability changes.
 Your blocks will be called immediately after the observer is added with the current reachability status.
 @param observer The observer that wants to register for reachability changes.
 @param reachableBlock Block to be called whenever the local network becomes reachable.
 @param unreachableBlock Block to be called whenever the local network becomes unreachable.
 */
- (void)addReachabilityObserverForInternet:(id)observer
                            reachableBlock:(AAFReachabilityLocalNetworkBecameReachable)reachableBlock
                          unreachableBlock:(AAFReachabilityLocalNetworkBecameUnreachable)unreachableBlock;

/**
 Remove an observer for internet reachability.
 @param observer The observer that wants to unregister for reachability changes.
 */
- (void)removeReachabilityObserverForInternet:(id)observer;

/**
 Add an observer for local network reachability and pass which blocks to call on reachability changes.
 Your blocks will be called immediately after the observer is added with the current reachability status.
 @param observer The observer that wants to register for reachability changes.
 @param reachableBlock Block to be called whenever the local network becomes reachable.
 @param unreachableBlock Block to be called whenever the local network becomes unreachable.
 */
- (void)addReachabilityObserverForLocalNetwork:(id)observer
                                reachableBlock:(AAFReachabilityLocalNetworkBecameReachable)reachableBlock
                              unreachableBlock:(AAFReachabilityLocalNetworkBecameUnreachable)unreachableBlock;

/**
 Remove an observer for local network reachability.
 @param observer The observer that wants to unregister for reachability changes.
 */
- (void)removeReachabilityObserverForLocalNetwork:(id)observer;

@end

NS_ASSUME_NONNULL_END
