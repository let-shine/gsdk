
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

extern NSString *const AAFWiFiUserInfoCarIdentifierKey;
extern NSString *const AAFWiFiUserInfoCarIdentifierTypeKey;

/**
 Model object that holds information about a WiFI
 */

@interface AAFWiFi : NSObject <NSCopying>

/**
 NSString representation of the current IP address.
 */
@property (nonatomic, copy, readonly) NSString *wifiIPAddress;

/**
 NSString representation of the current subnet.
 */
@property (nonatomic, copy, readonly) NSString *wifiIPSubnet;

/**
 NSString representation of the WiFi's SSID.
 */
@property (nonatomic, copy, readonly, nullable) NSString *wifiSSIDAddress;

/**
 NSString representation of the WiFi's BSSID.
 */
@property (nonatomic, copy, readonly, nullable) NSString *wifiBSSIDAddress;

/**
 NSString representation of the WiFi's network interface.
 */
@property (nonatomic, copy, readonly) NSString *wifiNetworkInterface;

/**
 Boolean representing if the current WiFi is a Personal Hotspot.
 */
@property (nonatomic, assign, readonly, getter=isPersonalHotspot) BOOL personalHotspot;

/**
 Boolean indicating whether the WiFi network is a car WiFi
 */
@property (nonatomic, assign, readonly, getter=isCarWiFi) BOOL carWiFi;

/**
 Boolean indicating whether the WiFi networks SSID and BSSID are unknown
 */
@property (nonatomic, assign, readonly, getter=hasUnknownSSID) BOOL unknownSSID;

/**
 A string that uniquely identifies this WiFi. This is typically the BSSID, but can be a supplemental UUID if the BSSID is unavailable.
 */
@property (nonatomic, copy, readonly) NSString *uniqueIdentifier;

/**
 A dictionary that can be used to store arbitrary additional information about the WiFi
 */
@property (nonatomic, copy, readonly) NSDictionary<NSString *, id> *userInfo;

- (BOOL)isEqualToWiFi:(AAFWiFi *__nullable)wifi;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
