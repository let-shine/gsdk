
@import Foundation;
#import <NetworkInformation/AAFWiFi.h>

NS_ASSUME_NONNULL_BEGIN

@class AAFWiFiManager;

/**
 Classes adopting this protocol can be informed about changes to the WiFi network the device is connected to.
 */
@protocol AAFWiFiObserver<NSObject>
@optional

/**
 Called when the WiFi interface changes.
 @param manager The WiFi manager where this call originated from
 */
- (void)wifiManagerDidChangeWiFiEnabled:(AAFWiFiManager *)manager;

/**
 Called when the currently connected WiFi changes or when the device disconnects.
 @param manager The WiFi manager where this call originated from
 @param wifi The new WiFi network that has changed or was connected. nil indicates that the WiFi connection was lost
 */
- (void)wifiManager:(AAFWiFiManager *)manager didChangeCurrentWiFi:(AAFWiFi *__nullable)wifi;

/**
 Called when the currently connected WiFi was promoted to a car WiFi
 @param manager The WiFi manager where this call originated from
 @param wifi The new WiFi network that was promoted to a car WiFi
 */
- (void)wifiManager:(AAFWiFiManager *)manager currentWiFiDidBecomeCarWiFi:(AAFWiFi *)wifi;

/**
 Called when a value in the userInfo dictionary of the currently connected WiFi changed
 @param manager The WiFi manager where this call originated from
 @param wifi The new WiFi network that changed its userInfo
 @param userInfoKey The key inside the userInfo dictionary that changed
 */
- (void)wifiManager:(AAFWiFiManager *)manager currentWiFi:(AAFWiFi *)wifi didChangeValueForUserInfoKey:(NSString *)userInfoKey;

@end

/**
 This class allows an app to get information about the current WiFi and react on any network changes
 */
@interface AAFWiFiManager : NSObject

/**
 @return YES if the device is connected to a WiFi network.
 */
@property (atomic, assign, readonly, getter=isWiFiConnected) BOOL wiFiConnected;

/**
 @return YES if WiFi radio on the device is turned on.
 */
@property (atomic, assign, readonly, getter=isWiFiEnabled) BOOL wiFiEnabled;

/**
 @return AAFWiFi object of the WiFi the device is connected to, else nil.
 */
@property (atomic, strong, readonly, nullable) AAFWiFi *currentWiFi;

/**
 Default WiFi observer instance
 */
+ (AAFWiFiManager *)defaultWiFiManager;

/**
 Adds the observer for WiFi changes.
 If this object ceases to exist the notification is not called anymore.
 The observer is always notified in the main thread 
 @param observer The observer whose existince determines if the notification is called.
 */
- (void)addObserver:(id<AAFWiFiObserver>)observer;

/**
 Removes the observer for WiFi changes.
 @param observer The observer to remove from the list of observers that will be called on WiFi changes.
 */
- (void)removeObserver:(id<AAFWiFiObserver>)observer;

/**
 Updates a value in the currently connected WiFi network's userInfo dictionary and notifies all observers of the change
 @param value The new value to be associated with the key
 @param key   The whose value should be set
 */
- (void)setValue:(id)value forCurrentWiFiUserInfoKey:(NSString *)key;

/**
 Marks the current WiFi as being a car WiFi and notifies all observers about this
 */
- (void)setCurrentWiFiIsCarWiFi;

/**
 This is for debug purposes
 */
- (void)resetWiFiStorage;

@end

NS_ASSUME_NONNULL_END
