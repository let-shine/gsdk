
@import Foundation;
@import CoreGraphics;

NS_ASSUME_NONNULL_BEGIN

@interface AAFCalendar : NSObject

@property (nonatomic, copy, readonly) NSString *identifier;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, assign, readonly) CGColorRef CGColor;
@property (nonatomic, assign, readonly, getter=isBirthdayCalendar) BOOL birthdayCalendar;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
