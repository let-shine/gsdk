
@import EventKit;
@import PlaceKit;
#import <PersonalInformation/AAFContact.h>
#import <PersonalInformation/AAFCalendar.h>
#import <PersonalInformation/AAFCalendarEventAttendee.h>

NS_ASSUME_NONNULL_BEGIN

@interface AAFCalendarEvent : AAFPlaceItem

@property (nonatomic, strong, readonly) AAFCalendar *calendar;

/**
 Returns YES if this is an instance of AAFCalendarEventMultiDay
 Do not use this for displaying
 */
@property (nonatomic, assign, readonly, getter=isPartOfMultidayEvent) BOOL partOfMultidayEvent;

@property (nonatomic, strong, readonly) NSDate *startDate;
@property (nonatomic, strong, readonly) NSDate *endDate;

@property (nonatomic, copy, readonly, nullable) NSURL *URL;

@property (nonatomic, copy, readonly, nullable) NSString *location;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, assign, readonly, getter=isAllDay) BOOL allDay;
@property (nonatomic, assign, readonly, getter=isDetachedFromRecurrentEvent) BOOL detachedFromRecurrentEvent;
@property (nonatomic,  assign, readonly, getter=isPartOfRecurrentEvent) BOOL partOfRecurrentEvent;

/**
 Returns true if the underlying calendar event ends on the same day as is starts. This is also true for events ending at the start of the next day (e.g. 00:00) if isPartOfMultidayEvent is YES.
 */
@property (nonatomic, assign, readonly) BOOL endsOnSameDay;
@property (nonatomic, copy, readonly, nullable) NSString *notes;

@property (nonatomic, copy, readonly) NSString *identifier;

@property (nonatomic, strong, readonly, nullable) AAFCalendarEventAttendee *organizer;
@property (nonatomic, strong, readonly) NSArray<AAFCalendarEventAttendee *> *attendees;

/**
 Returns the identifier of the underlying EKEvent. This differs from `identifier` because split events have different values for `identifier` even if they are based on the same `EKEvent` instance.
*/
@property (nonatomic, strong, readonly) NSString *eventIdentifier;

- (instancetype)init NS_UNAVAILABLE;

- (NSComparisonResult)compare:(AAFCalendarEvent *)event;

@end

NS_ASSUME_NONNULL_END
