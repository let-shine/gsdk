
#import <PersonalInformation/AAFContact.h>

typedef NS_ENUM(NSUInteger, AAFCalendarEventAttendeeStatus) {
    AAFCalendarEventAttendeeStatusUnknown,
    AAFCalendarEventAttendeeStatusPending,
    AAFCalendarEventAttendeeStatusAccepted,
    AAFCalendarEventAttendeeStatusDeclined,
    AAFCalendarEventAttendeeStatusTentative,
    AAFCalendarEventAttendeeStatusDelegated,
    AAFCalendarEventAttendeeStatusCompleted,
    AAFCalendarEventAttendeeStatusInProcess
};


NS_ASSUME_NONNULL_BEGIN
@interface AAFCalendarEventAttendee : NSObject

@property (nonatomic, strong, readonly) AAFContact *contact;
@property (nonatomic, assign, readonly) AAFCalendarEventAttendeeStatus status;
@property (nonatomic, assign, readonly, getter=isOrganizer) BOOL organizer;
@property (nonatomic, copy, readonly, nullable) NSString *name;
@property (nonatomic, copy, readonly) NSString *emailAddress;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
