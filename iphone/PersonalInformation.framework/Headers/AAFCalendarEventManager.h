
#import <PersonalInformation/AAFCalendar.h>
#import <PersonalInformation/AAFCalendarEvent.h>
#import <PersonalInformation/AAFCalendarEventSearchQuery.h>
#import <PersonalInformation/AAFCalendarEvents.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const AAFCalendarEventManagerItemsDidChangeNotification;
extern NSString * const AAFCalendarEventManagerAccessDeniedNotification;
extern NSString * const AAFCalendarEventManagerAccessGrantedNotification;


@interface AAFCalendarEventManager : NSObject

@property (nonatomic, strong, readonly) NSArray<AAFCalendar *> *allEventCalendars;
@property (nonatomic, assign, readonly) BOOL hasAuthorizedEventAccess;

#pragma mark - Events

+ (instancetype)defaultManager;

- (AAFCalendarEvents *)allCalendarEventsAroundNow;

- (AAFCalendarEvents *)allCalendarEventsFromToday;

- (AAFCalendarEvents *)allCalendarEventsFromTodayWithoutMultidayEvents;

- (AAFCalendarEvents *)lastAndNextEventsAroundNow;

- (AAFCalendarEvents *)allCalendarNext24Hour;

- (AAFCalendarEvents *)nextEventsAfterEvent:(AAFCalendarEvent *)event;

- (AAFCalendarEvents *)nextCalendarEventsFromToday;

- (AAFCalendarEvents *)allCalendarEventsFromStartDate:(NSDate *)startDate
                                            toEndDate:(NSDate *)endDate;

- (AAFCalendarEvents *)allEventsFromStartDate:(NSDate *)startDate
                                    toEndDate:(NSDate *)endDate
                                  inCalendars:(nullable NSArray<AAFCalendar *> *)calendars;

- (nullable AAFCalendarEvents *)allCalendarEventsBetweenThePast:(NSUInteger)numberOfPastEvents
                                                   toNextEvents:(NSUInteger)numberOfNextEvents;

- (AAFCalendarEvents *)allCalendarPastEventsFrom:(AAFCalendarEvent *)calendarEvent
                                      eventCount:(NSUInteger)numberOfPastEvents;

- (AAFCalendarEvents *)allPastEventsFrom:(AAFCalendarEvent *)calendarEvent
                              eventCount:(NSUInteger)numberOfPastEvents
                             inCalendars:(nullable NSArray<AAFCalendar *> *)calendars;

- (AAFCalendarEvents *)allCalendarNextEventsFrom:(AAFCalendarEvent *)calendarEvent
                                      eventCount:(NSUInteger)numberOfNextEvents;

- (AAFCalendarEvents *)allNextEventsFrom:(AAFCalendarEvent *)calendarEvent
                              eventCount:(NSUInteger)numberOfNextEvents
                             inCalendars:(nullable NSArray<AAFCalendar *> *)calendars;

/// get at least the 'minCount' next or past events for the specified date, if there are that many.
/// if future events are requested, events are included which start at the specified date. if past events are requested, only events before the date are included
/// @param date the date to search from.
/// @param eventMinCount the minimum number of evetns to get, if possibleget the next(+) or past(-) 'eventCount' events depending on the sign
/// @param searchForPastEvetns determine if events should be searched in the past or future.
/// @param calendars specify from which calendars events should be retrieved. if nil or empty all calendars are used
- (AAFCalendarEvents *)eventsForDate:(NSDate *)date
                            minCount:(NSUInteger)eventMinCount
                          pastEvents:(BOOL)searchForPastEvetns
                           calendars:(nullable NSArray<AAFCalendar *> *)calendars;

- (AAFCalendarEventSearchQuery *)searchCalendarEventsForSearchTerm:(NSString *)searchTerm
                                                 completionHandler:(AAFCalendarEventSearchCompletionHandler)completionHandler;


/**
 Allows refetching of events
 
 @param identifiers An array of AAFCalendarEvent identifiers
 @return An AAFCalendarEvents instance containing all events with the passed identifiers. May contain less or more events than identifiers depending on the performed modifications to the underlying data store.
 */
- (AAFCalendarEvents *)eventsWithIdentifiers:(NSArray<NSString *> *)identifiers;

@end

NS_ASSUME_NONNULL_END
