
#import <PersonalInformation/AAFCalendarEvent.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, AAFCalendarEventMultiDayType)
{
    AAFCalendarEventMultiDayTypeStart,
    AAFCalendarEventMultiDayTypeMiddle,
    AAFCalendarEventMultiDayTypeEnd,
    /// Event end date has been cropped to prevent overlap into the next day (used for events that end exactly at 00:00 of the following day)
    AAFCalendarEventMultiDayTypeCropped,
};

@interface AAFCalendarEventMultiDay : AAFCalendarEvent

@property (nonatomic, assign, readonly) AAFCalendarEventMultiDayType type;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
