
@import Foundation;
#import <PersonalInformation/AAFCalendarEvents.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^AAFCalendarEventSearchCompletionHandler)(AAFCalendarEvents *foundEvents);

@interface AAFCalendarEventSearchQuery : NSObject

/// Set to YES to cancel a running search.
@property (atomic, assign, readwrite, getter=isCanceled) BOOL cancel;

- (instancetype)init NS_UNAVAILABLE;

@end

NS_ASSUME_NONNULL_END
