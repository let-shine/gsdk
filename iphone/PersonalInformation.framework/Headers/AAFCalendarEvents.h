
#import <PersonalInformation/AAFCalendarEvent.h>
#import <PersonalInformation/AAFCalendarEventMultiDay.h>

/**
 A collection of @c AAFCalendarEvent objects.
*/
@interface AAFCalendarEvents : NSObject

/**
 @return events
 */
@property (nonatomic, readonly, strong) NSOrderedSet<AAFCalendarEvent *> *events;

/**
 @return events sorted by startDate ascending
 */
@property (nonatomic, readonly, strong) NSOrderedSet<AAFCalendarEvent *> *sortedEvents;

/**
 Split a multi-day event into multiple single-day events. The @c type property of multi-day events indicates whether the event is at the start, middle, or end of the multi-day event. 
 @return Split events sorted by startDate ascending
 */
@property (nonatomic, readonly, strong) NSOrderedSet<__kindof AAFCalendarEvent *> *splitEvents;

- (instancetype)init NS_UNAVAILABLE;

/// @return a new AAFCalendarEvents object with the provided calendar event set.
+ (instancetype)eventsWithOrderedSet:(NSOrderedSet<AAFCalendarEvent *> *)calendarEventSet;

/// return the next or past 'eventCount' split events form the specified date, depending on splitPastEvents
/// events are sorted by startDate ascending
- (NSOrderedSet<__kindof AAFCalendarEvent *> *)splitEventsFromDate:(NSDate *)date
                                                       intoThePast:(BOOL)splitPastEvents
                                                        eventCount:(NSUInteger)eventCount;

/// return all split events which start within the specified date range.
/// events are sorted by startDate ascending
- (NSOrderedSet<__kindof AAFCalendarEvent *> *)splitEventsFromStartDate:(NSDate *)startDate
                                                              toEndDate:(NSDate *)endDate;

/**
 Use this method to remove all recurrent events with the exception of the first.
 This method used sortedEvents for grouping the events.

 @param includeDetachedEvents Set to true to include recurrent events that are detached from the original event.
 @return An AAFCalendarEvents instead that includes only the first occurence of a recurrent event.
 */
- (AAFCalendarEvents *)calendarEventsByGroupingRecurrentEvents:(BOOL)includeDetachedEvents;

@end

@interface AAFCalendarEvents (Deprecated)

@property (nonatomic, readonly, strong) NSOrderedSet<AAFCalendarEvent *> *original DEPRECATED_MSG_ATTRIBUTE("Use events instead");
@property (nonatomic, readonly, strong) NSOrderedSet<__kindof AAFCalendarEvent *> *splitItems DEPRECATED_MSG_ATTRIBUTE("Use splitEvents instead");
@property (nonatomic, readonly, strong) NSOrderedSet<__kindof AAFCalendarEvent *> *splittedEvents DEPRECATED_MSG_ATTRIBUTE("Use splitEvents instead");


@end
