
@import PlaceKit;
#import <PersonalInformation/AAFContactLabeledValue.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Handler is called on the dispatch_queue_t return by dispatch_get_global_queue for QOS_CLASS_USER_INITIATED.
 */
typedef void (^AAFContactImageLoadCompletionHandler)(UIImage *__nullable image);


@interface AAFContact : AAFPlaceItem

@property (nonatomic, copy, readonly, nullable) NSString *identifier;

@property (nonatomic, copy, readonly, nullable) NSString *givenName;
@property (nonatomic, copy, readonly, nullable) NSString *familyName;

/**
 If givenName and familyName are available, this returns a formatted name using NSPersonNameComponentsFormatter.
 If none of this is set, it will fall back to company or nickName.
 */
@property (nonatomic, copy, readonly, nullable) NSString *formattedName;

@property (nonatomic, copy, readonly, nullable) NSString *company;
@property (nonatomic, copy, readonly, nullable) NSString *nickName;

@property (nonatomic, strong, readonly, nullable) NSArray<NSString *> *telephoneNumbers;
@property (nonatomic, strong, readonly, nullable) NSArray<NSURL *> *urls;
@property (nonatomic, strong, readonly, nullable) NSArray<NSString *> *emails;

@property (nonatomic, strong, readonly, nullable) NSArray<AAFContactLabeledValue<NSString *> *> *labeledTelephoneNumbers;
@property (nonatomic, strong, readonly, nullable) NSArray<AAFContactLabeledValue<NSURL *> *> *labeledURLs;
@property (nonatomic, strong, readonly, nullable) NSArray<AAFContactLabeledValue<NSString *> *> *labeledEmails;

@property (nonatomic, copy, readonly) NSString *sortKey;

@property (nonatomic, strong, readonly, nullable) UIImage *thumbnailImage;
@property (nonatomic, assign, readonly, getter=isThumbnailImageCached) BOOL thumbnailImageCached;
@property (nonatomic, strong, readonly, nullable) UIImage *image;
@property (nonatomic, assign, readonly, getter=isImageCached) BOOL imageCached;

- (instancetype)init NS_UNAVAILABLE;

- (void)loadImageWithCompletionHandler:(AAFContactImageLoadCompletionHandler)completionHandler;
- (void)loadThumbnailImageWithCompletionHandler:(AAFContactImageLoadCompletionHandler)completionHandler;

@end

NS_ASSUME_NONNULL_END
