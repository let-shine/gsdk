
@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@interface AAFContactLabeledValue<ValueType : id <NSCopying, NSSecureCoding>> : NSObject

@property (nonatomic, copy, readonly, nullable) NSString *label;
@property (nonatomic, copy, readonly) ValueType value;

@end

NS_ASSUME_NONNULL_END
