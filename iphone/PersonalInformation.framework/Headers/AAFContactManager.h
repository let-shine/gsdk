
@import AddressBook;
#import <PersonalInformation/AAFContact.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString * const AAFContactManagerAccessDeniedNotification;
extern NSString * const AAFContactManagerItemsDidChangeNotification;

@interface AAFContactManager : NSObject

@property (nonatomic, assign, readonly) CNAuthorizationStatus addressBookAccessAuthorizationStatus;

+ (instancetype)defaultManager;

- (NSArray<AAFContact *> *)allContactsSortedByName;
- (NSSet<AAFContact *> *)allContacts;

@end

NS_ASSUME_NONNULL_END
