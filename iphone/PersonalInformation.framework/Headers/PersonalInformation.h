
@import Foundation;
#import <PersonalInformation/AAFContact.h>
#import <PersonalInformation/AAFContactManager.h>
#import <PersonalInformation/AAFCalendar.h>
#import <PersonalInformation/AAFCalendarEvent.h>
#import <PersonalInformation/AAFCalendarEventAttendee.h>
#import <PersonalInformation/AAFCalendarEventManager.h>
#import <PersonalInformation/AAFCalendarEventMultiDay.h>
#import <PersonalInformation/AAFCalendarEventSearchQuery.h>
#import <PersonalInformation/AAFCalendarEvents.h>
