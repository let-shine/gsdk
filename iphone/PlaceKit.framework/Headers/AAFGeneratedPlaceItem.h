
#import <PlaceKit/AAFPlaceItem.h>

NS_ASSUME_NONNULL_BEGIN

@interface AAFGeneratedPlaceItem : AAFPlaceItem

@property (nonatomic, copy, readwrite, nullable) NSNumber *rating;
@property (nonatomic, copy, readwrite, nullable) NSString *itemDescription;

- (instancetype)initWithTitle:(NSString *)title
                      address:(AAFAddress *)address;

- (instancetype)initWithTitle:(NSString *)title
                     location:(CLLocation *)location;

- (instancetype)initWithTitle:(NSString *)title
                      address:(AAFAddress *)address
                     location:(CLLocation *)location;

- (void)updateTitle:(NSString *)newTitle;

@end

NS_ASSUME_NONNULL_END
