
@import CoreLocation;
@import GeoKit;
#import <PlaceKit/AAFGeneratedPlaceItem.h>
#import <PlaceKit/AAFPlaceItem.h>

NS_ASSUME_NONNULL_BEGIN

extern NSString *const AAFPlaceErrorDomain;

typedef NS_ERROR_ENUM(AAFPlaceErrorDomain, AAFPlaceError) {
    AAFPlaceErrorNoPlacemarksFound = -1000,
    AAFPlaceErrorGeocoderFailed = -1001,
};

typedef void (^AAFPlacePlacemarkCompletionHandler)(AAFPlacemark *__nullable placemark, NSError *__nullable error);

/**
 * AAFPlace is the abstraction of a place. A place doesn't need to have a location.
 * This class always has a corresponding Item (AAFPlaceItem) which is the data entity.
 * You should never allocate this class directly - this is one of the objects that is returned by the corresponding item.
 */
@interface AAFPlace : NSObject

/**
 A placeholder string that can be used to display the place before its placemark has been resolved using placemarkWithCompletionHandler
 */
@property (nonatomic, strong, readonly, nullable) NSString *placeholderString;

/**
 The most recent result from placemarkWithCompletionHandler:. This property is null until the first time placemarkWithCompletionHandler: did return without error.
 */
@property (nonatomic, strong, readonly, nullable) AAFPlacemark *cachedPlacemark;

- (instancetype)init NS_UNAVAILABLE;

/** 
 * An AAFPlace always has a corresponding Item. The AAFPlaceItem can hold no, one or more AAFPlaces.
 */
- (AAFPlaceItem *)correspondingItem;

/**
 * The placemark is always fetched asyncroniosly. The completion handler is always called, but if the location/address cannot be resolved it will not return a placemark.
 */
- (void)placemarkWithCompletionHandler:(AAFPlacePlacemarkCompletionHandler)completionHandler;

@end

NS_ASSUME_NONNULL_END
