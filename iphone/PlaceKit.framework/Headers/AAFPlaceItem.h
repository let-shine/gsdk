
@import Foundation;
@import GeoKit;

@class AAFPlace;

NS_ASSUME_NONNULL_BEGIN

@interface AAFPlaceItem : NSObject

// Array of AAFPlace objects automatically generated from the addresses and locations properties
@property (nonatomic, strong, readonly) NSArray<AAFPlace *> *places;

// Subclasses need to provide these properties
@property (nonatomic, strong, readonly, nullable) NSSet<AAFAddress *> *addresses;
@property (nonatomic, strong, readonly, nullable) NSSet<CLLocation *> *locations;

@property (nonatomic, copy, readonly, nullable) NSString *title;

@end

NS_ASSUME_NONNULL_END
